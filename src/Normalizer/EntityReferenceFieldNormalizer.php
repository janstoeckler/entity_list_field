<?php

namespace Drupal\entity_list_field\Normalizer;

use Drupal\Core\Language\LanguageInterface;
use Drupal\entity_list_field\Plugin\Field\EntityListItemList;
use Drupal\jsonapi\Normalizer\Relationship;
use Drupal\jsonapi\Normalizer\Value\NullFieldNormalizerValue;
use Drupal\jsonapi\Resource\EntityCollection;
use Drupal\jsonapi\Normalizer\EntityReferenceFieldNormalizer as OriginalEntityReferenceFieldNormalizer;

/**
 * Normalizer class specific for entity reference field objects.
 *
 * @internal
 */
class EntityReferenceFieldNormalizer extends OriginalEntityReferenceFieldNormalizer {

  /**
   * {@inheritdoc}
   */
  protected $supportedInterfaceOrClass = EntityListItemList::class;

  /**
   * {@inheritdoc}
   */
  public function normalize($field, $format = NULL, array $context = []) {
    /* @var \Drupal\Core\Field\FieldItemListInterface $field */

    $field_access = $field->access('view', $context['account'], TRUE);
    if (!$field_access->isAllowed()) {
      return new NullFieldNormalizerValue($field_access, 'relationships');
    }

    // Build the relationship object based on the Entity Reference and normalize
    // that object instead.
    $main_property = $field->getItemDefinition()->getMainPropertyName();
    $definition = $field->getFieldDefinition();
    $cardinality = $definition
      ->getFieldStorageDefinition()
      ->getCardinality();

    $langcode = \Drupal::service('language_manager')->getCurrentLanguage(LanguageInterface::TYPE_CONTENT)->getId();

    $entity_list = [];
    $entity_list_metadata = [];
    foreach (\Drupal::service('entity_list')->getEntities($field, $langcode) as $delta => $entity) {
      $entity_list[$delta] = $entity;
      $entity_list_metadata[$delta] = [
        'title' => $entity->label(),
      ];
    }

    $entity_collection = new EntityCollection($entity_list);
    $relationship = new Relationship($this->resourceTypeRepository, $field->getName(), $entity_collection, $field->getEntity(), $field_access, $cardinality, $main_property, $entity_list_metadata);
    return $this->serializer->normalize($relationship, $format, $context);
  }

}
