<?php

namespace Drupal\entity_list_field\Plugin\Field\FieldType;

use Drupal\Component\Utility\Random;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Field\Plugin\Field\FieldType\MapItem;
use Drupal\Core\Field\Plugin\Field\FieldType\StringItemBase;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\TypedData\DataReferenceTargetDefinition;
use Drupal\Core\TypedData\DataReferenceDefinition;
use Drupal\Core\Entity\TypedData\EntityDataDefinition;

/**
 * Defines the 'entity_list' entity field type.
 *
 * @FieldType(
 *   id = "entity_list",
 *   label = @Translation("Entity list"),
 *   description = @Translation("A field for configuring entity lists."),
 *   category = @Translation("Other"),
 *   list_class = "\Drupal\entity_list_field\Plugin\Field\EntityListItemList",
 *   default_widget = "entity_list_widget",
 *   default_formatter = "entity_list_formatter"
 * )
 */
class EntityListItem extends MapItem {

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    // This is called very early by the user entity roles field. Prevent
    // early t() calls by using the TranslatableMarkup.
    $properties['value'] = DataDefinition::create('any')
      ->setLabel(new TranslatableMarkup('Value'));

    $properties['target_id'] = DataReferenceTargetDefinition::create('integer')
      ->setLabel(new TranslatableMarkup('@label ID', ['@label' => 'Foo']))
      ->setSetting('unsigned', TRUE);

    $settings = $field_definition->getSettings();
    $target_type_info = \Drupal::entityTypeManager()->getDefinition($settings['target_type']);

    $properties['entity'] = DataReferenceDefinition::create('entity_revision')
      ->setLabel($target_type_info->getLabel())
      ->setDescription(t('The referenced entity revision'))
      // The entity object is computed out of the entity ID.
      ->setComputed(TRUE)
      ->setReadOnly(FALSE)
      ->setTargetDefinition(EntityDataDefinition::create($settings['target_type']));

    return $properties;
  }

  /**
   * Determines whether the item holds an unsaved entity.
   *
   * This is notably used for "autocreate" widgets, and more generally to
   * support referencing freshly created entities (they will get saved
   * automatically as the hosting entity gets saved).
   *
   * @return bool
   *   TRUE if the item holds an unsaved entity.
   */
  public function hasNewEntity() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public static function mainPropertyName() {
    // A map item has no main property.
    return 'target_id';
  }

}

