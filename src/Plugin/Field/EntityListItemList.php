<?php

namespace Drupal\entity_list_field\Plugin\Field;

use Drupal\Core\Field\MapFieldItemList;

/**
 * Defines a item list class for map fields.
 */
class EntityListItemList extends MapFieldItemList {

}
