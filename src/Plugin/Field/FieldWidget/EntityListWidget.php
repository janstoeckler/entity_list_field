<?php

namespace Drupal\entity_list_field\Plugin\Field\FieldWidget;

use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\NestedArray;
use Drupal\Component\Utility\SortArray;
use Drupal\Core\Entity\Element\EntityAutocomplete;
use Drupal\Core\Field\FieldFilteredMarkup;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\link\LinkItemInterface;
use Drupal\link\Plugin\Field\FieldWidget\LinkWidget;

/**
 * Plugin implementation of the 'entity_list' widget.
 *
 * @FieldWidget(
 *   id = "entity_list_widget",
 *   label = @Translation("Entity list"),
 *   field_types = {
 *     "entity_list"
 *   }
 * )
 */
class EntityListWidget extends WidgetBase {

  use AutomaticListTrait;
  use ManualReferenceTrait;

  /**
   * @inheritdoc
   */
  public function formElement(
    FieldItemListInterface $items,
    $delta,
    array $element,
    array &$form,
    FormStateInterface $form_state
  ) {

    // Get states & build identifiers.
    $field_name = $this->fieldDefinition->getName();
    $id = implode('-', array_merge($form['#parents'], [$field_name]));
    $wrapper = Html::getUniqueId($id . '-add-more');

    if (isset($items[$delta]->value)) {
      switch($items[$delta]->value['mode']) {
        case 'automatic':
          $settings = $items->getItemDefinition()->getSettings();
          $element['value'] = $this->automaticElement($wrapper, $items[$delta]->value, $settings);
          break;
        case 'manual':
          $element['value'] = $this->manualElement($wrapper, $items[$delta]->value);
          break;
      }
      $element['remove'] = $this->addRemoveButton($form, $wrapper, $id, $delta);
    }

    return $element;

  }

  /**
   * @inheritdoc
   */
  public static function addMoreSubmit(
    array $form,
    FormStateInterface $form_state
  ) {
    $button = $form_state->getTriggeringElement();
    $button_name = explode('_', $button['#name']);
    $mode = end($button_name);

    // Go one level up in the form, to the widgets container.
    $element = NestedArray::getValue($form, array_slice($button['#array_parents'], 0, -1));
    $field_name = $element['#field_name'];
    $parents = $element['#field_parents'];

    // Increment the items count.
    $field_state = static::getWidgetState($parents, $field_name, $form_state);
    $field_state['increment'] = $mode;
    $field_state['items_count']++;
    static::setWidgetState($parents, $field_name, $form_state, $field_state);

    $form_state->setRebuild();
  }

  /**
   * @inheritdoc
   */
  public static function addMoreAjax(
    array $form,
    FormStateInterface $form_state
  ) {
    $button = $form_state->getTriggeringElement();

    // Go three levels up in the form, to the widgets container.
    $element = NestedArray::getValue($form, array_slice($button['#array_parents'], 0, -1));

    return $element;
  }

  /**
   * @inheritdoc
   */
  protected function formMultipleElements(FieldItemListInterface $items, array &$form, FormStateInterface $form_state) {

    // Increment the items count.
    $field_state = static::getWidgetState($form['#parents'], $items->getName(), $form_state);

    if (isset($field_state['increment'])) {
      $mode = $field_state['increment'];
      $items->appendItem([
        'value' => [
          'mode' => $mode,
        ]
      ]);
    }

    if (isset($field_state['remove'])) {
      $delta = (int) $field_state['remove'];
      if($items->get($delta)) {
        $items->removeItem($delta);
      }
    }

    $elements = parent::formMultipleElements($items, $form, $form_state);

//    $wrapper_id = $elements['#prefix'];
    // Add appropriate ajax wrapper for replacing field widget.
    foreach ($items as $delta => $item) {
      $elements[$delta]['remove']['#ajax']['wrapper'] = $elements['add_more']['#ajax']['wrapper'];
    }

//    $id_prefix = implode('-', array_merge($parents = $form['#parents'], [$this->fieldDefinition->getName()]));
//    $wrapper_id = $id_prefix . '-add-more-wrapper';
//    $elements['#prefix'] = '<div id="' . $wrapper_id . '">';

    $elements['#theme'] = 'field_multiple_value_form_entity_list_field';
    if (isset($elements['add_more'])) {
      $elements['add_more_automatic'] = $this->addButton($elements['add_more'], 'automatic', 'Add automatic entity list');
//      $elements['add_more_automatic']['#ajax']['wrapper'] = $wrapper_id;
      $elements['add_more_manual'] = $this->addButton($elements['add_more'], 'manual', 'Add manual entity reference');
//      $elements['add_more_manual']['#ajax']['wrapper'] = $wrapper_id;
    }

    foreach ($items as $key => $item) {
      if (!isset($elements[$key]['value'])) {
        unset($elements[$key]);
      }
    }

    $elements['#attached']['library'][] = 'entity_list_field/widget';

    return $elements;
  }

  /**
   * Add add buttons.
   *
   * @param $button
   *   The button element to copy.
   * @param $mode
   *   The button mode to add.
   * @param $title
   *   The button title.
   *
   * @return mixed
   */
  protected function addButton($button, $mode, $title) {
    $button['#value'] = $this->t($title);
    $button['#name'] = $button['#name'] . '_' . $mode;
    return $button;
  }

  protected function addRemoveButton($form, $wrapper, $id, $delta) {
    $button = [
      '#type' => 'submit',
      '#name' => $id . '-remove-' . $delta,
      '#value' => $this->t('Remove'),
      '#attributes' => [
        'class' => [
          'field-remove-submit',
        ],
      ],
      '#submit' => [
        [
          static::class,
          'removeSubmit',
        ]
      ],
      '#ajax' => [
        'callback' => [
          static::class,
          'removeAjax',
        ],
        'wrapper' => $id . '-add-more-wrapper',
        'effect' => 'fade',
      ],
      '#weight' => -999,
    ];

    return $button;
  }

  /**
   * {@inheritdoc}
   */
  public function extractFormValues(FieldItemListInterface $items, array $form, FormStateInterface $form_state) {
    $field_name = $this->fieldDefinition->getName();

    // Extract the values from $form_state->getValues().
    $path = array_merge($form['#parents'], [$field_name]);
    $key_exists = NULL;
    $values = NestedArray::getValue($form_state->getValues(), $path, $key_exists);

    if ($key_exists) {
      // Account for drag-and-drop reordering if needed.
      if (!$this->handlesMultipleValues()) {
        // Remove the 'value' of the 'add more' button.
        unset($values['add_more']);
        unset($values['add_more_automatic']);
        unset($values['add_more_manual']);

        // The original delta, before drag-and-drop reordering, is needed to
        // route errors to the correct form element.
        foreach ($values as $delta => &$value) {
          $value['_original_delta'] = $delta;
        }

        usort($values, function ($a, $b) {
          return SortArray::sortByKeyInt($a, $b, '_weight');
        });
      }

      // Let the widget massage the submitted values.
      $values = $this->massageFormValues($values, $form, $form_state);

      // Assign the values and remove the empty ones.
      $items->setValue($values);
      $items->filterEmptyItems();

      // Put delta mapping in $form_state, so that flagErrors() can use it.
      $field_state = static::getWidgetState($form['#parents'], $field_name, $form_state);
      foreach ($items as $delta => $item) {
        $field_state['original_deltas'][$delta] = isset($item->_original_delta) ? $item->_original_delta : $delta;
        unset($item->_original_delta, $item->_weight);
      }
      static::setWidgetState($form['#parents'], $field_name, $form_state, $field_state);
    }
  }

  protected function getCurrentFormHostEntity(FormStateInterface $form_state) {
    return $form_state->getFormObject()->getEntity();
  }

  /**
   * @inheritdoc
   */
  public static function removeSubmit(array $form, FormStateInterface $form_state) {
    $button = $form_state->getTriggeringElement();
    $button_name = explode('-', $button['#name']);
    $delta = end($button_name);

    // Go two levels up in the form, to the widgets container.
    $element = NestedArray::getValue($form, array_slice($button['#array_parents'], 0, -2));
    $field_name = $element['#field_name'];
    $parents = $element['#field_parents'];

    // Add the items count.
    $field_state = static::getWidgetState($parents, $field_name, $form_state);
    $field_state['remove'] = $delta;
    if ($field_state['items_count'] > 0) {
      $field_state['items_count']--;
    }
    static::setWidgetState($parents, $field_name, $form_state, $field_state);

    $user_input = $form_state->getUserInput();
    $field_specific_user_input = NestedArray::getValue($user_input, array_slice($button['#parents'], 0, -2));
    if (isset($field_specific_user_input[$delta])) {
      unset($field_specific_user_input[$delta]);
      $field_specific_user_input = array_values($field_specific_user_input);
    }
    NestedArray::setValue($user_input, array_slice($button['#parents'], 0, -2), $field_specific_user_input);
    $form_state->setUserInput($user_input);

    $form_state_values = $form_state->getValues();
    $field_specific_form_state_values = NestedArray::getValue($form_state_values, array_slice($button['#parents'], 0, -2));
    if (isset($field_specific_form_state_values[$delta])) {
      unset($field_specific_form_state_values[$delta]);
      $field_specific_form_state_values = array_values($field_specific_form_state_values);
    }
    NestedArray::setValue($form_state_values, array_slice($button['#parents'], 0, -2), $field_specific_form_state_values);
    $form_state->setValues($form_state_values);

    $form_state->setRebuild();
  }

  /**
   * @inheritdoc
   */
  public static function removeAjax(array $form, FormStateInterface $form_state) {
    $button = $form_state->getTriggeringElement();

    // Go two levels up in the form, to the widgets container.
    $element = NestedArray::getValue($form, array_slice($button['#array_parents'], 0, -2));

    return $element;
  }

}
