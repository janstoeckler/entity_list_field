<?php

namespace Drupal\entity_list_field\Plugin\Field\FieldWidget;

use Drupal\Core\Entity\Element\EntityAutocomplete;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\link\LinkItemInterface;
use Drupal\link\Plugin\Field\FieldWidget\LinkWidget;

trait AutomaticListTrait {

  /**
   * @param $wrapper
   * @param $value
   *
   * @return array
   */
  protected function automaticElement($wrapper, $value, $settings) {
    $types = [];
    foreach (\Drupal::service('entity_type.manager')->getDefinitions() as $id => $type) {
      $types[$id] = $type->getLabel();
    }

    $automatic = [
      '#prefix' => '<div id="' . $wrapper . '-automatic">',
      '#suffix' => '</div>',
      '#type' => 'container',
      '#attributes' => [
        'class' => [
          'automatic',
          'row',
        ],
      ],
      'mode' => [
        '#type' => 'value',
        '#tree' => TRUE,
        '#value' => 'automatic',
      ],
    ];

    $automatic['type'] = [
      '#type' => 'value',
      '#value' => $settings['target_type'],
    ];

    $bundles = [];
    foreach (\Drupal::service('entity_type.bundle.info')->getBundleInfo($settings['target_type']) as $id => $bundle) {
      if (in_array($id, $settings['bundles'], TRUE)) {
        $bundles[$id] = $bundle['label'];
      }
    }

    $automatic['bundle'] = [
      '#type' => 'select',
      '#title' => $this->t('Bundle'),
      '#default_value' => isset($value['bundle']) ? $value['bundle'] : $settings['default_bundle'],
      '#options' => $bundles,
    ];

    $automatic['sort'] = [
      '#type' => 'select',
      '#title' => $this->t('Sort'),
      '#default_value' => isset($value['sort']) ? $value['sort'] : 'date_desc',
      '#options' => [
        'date_asc' => $this->t('Date ascending'),
        'date_desc' => $this->t('Date descending'),
        'name_asc' => $this->t('Title ascending'),
        'name_desc' => $this->t('Title descending'),
      ],
    ];

    if ($settings['filter']['taxonomy_term']) {
      foreach ($settings['filter']['taxonomy_term'] as $taxonomy_bundle => $widget) {
        switch($widget) {
          case 'select':
            $options = [];
            $terms = \Drupal::service('entity_type.manager')->getStorage('taxonomy_term')->loadByProperties(['vid' => $taxonomy_bundle]);
            foreach ($terms as $term_id => $term) {
              $options[$term_id] = $term->label();
            }
            // Build tags select.
            $automatic['filter'][$taxonomy_bundle] = [
              '#type' => 'select',
              '#title' => \Drupal::service('entity_type.manager')->getStorage('taxonomy_vocabulary')->load($taxonomy_bundle)->label(),
              '#default_value' => isset($value['filter'][$taxonomy_bundle]) ? $value['filter'][$taxonomy_bundle] : null,
              '#options' => [0 => t('- None -')] + $options,
            ];
            break;
          case 'autocomplete':
            // Load term entities.
            if ($value['filter'][$taxonomy_bundle]) {
              foreach ($value['filter'][$taxonomy_bundle] as $delta => $item) {
                $value['filter'][$taxonomy_bundle][$delta] = \Drupal::service('entity_type.manager')->getStorage('taxonomy_term')->load($item['target_id']);
              }
            }
            // Build tags autocomplete.
            $automatic['filter'][$taxonomy_bundle] = [
              '#type' => 'entity_autocomplete',
              '#tags' => TRUE,
              '#title' => \Drupal::service('entity_type.manager')->getStorage('taxonomy_vocabulary')->load($taxonomy_bundle)->label(),
              '#default_value' => isset($value['filter'][$taxonomy_bundle]) ? $value['filter'][$taxonomy_bundle] : null,
              '#selection_settings' => [
                'target_bundles' => [
                  $taxonomy_bundle => $taxonomy_bundle,
                ],
                'match_operator' => 'CONTAINS'
              ],
              '#selection_handler' => 'default',
              '#target_type' => 'taxonomy_term',
            ];
            break;
        }
      }
    }

    $automatic['count'] = [
      '#type' => 'number',
      '#title' => $this->t('Count'),
      '#default_value' => isset($value['count']) ? $value['count'] : 3,
    ];

    $automatic['offset'] = [
      '#type' => 'number',
      '#title' => $this->t('Offset'),
      '#default_value' => isset($value['offset']) ? $value['offset'] : 0,
    ];

    return $automatic;
  }
}
