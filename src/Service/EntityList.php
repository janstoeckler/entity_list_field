<?php

namespace Drupal\entity_list_field\Service;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\TranslatableInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use Drupal\entity_list_field\Plugin\Field\FieldType\EntityListItem;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;

/**
 * Class EntityList.
 */
class EntityList {

  protected function getSortOptions() {
    return [
      'date_asc' => [
        'label' => new TranslatableMarkup('Date ascending'),
        'field' => 'created',
        'direction' => 'ASC',
      ],
      'date_desc' => [
        'label' => new TranslatableMarkup('Date descending'),
        'field' => 'created',
        'direction' => 'DESC',
      ],
      'name_asc' => [
        'label' => new TranslatableMarkup('Title ascending'),
        'field' => 'title',
        'direction' => 'ASC',
      ],
      'name_desc' => [
        'label' => new TranslatableMarkup('Title descending'),
        'field' => 'title',
        'direction' => 'DESC',
      ],
    ];
  }

  protected function getSort($key) {
  $sort_options = $this->getSortOptions();
  return [
    'field' => $sort_options[$key]['field'],
    'direction' => $sort_options[$key]['direction'],
  ];
}

  public function getSortLabels() {
    $sort_options = $this->getSortOptions();
    $options = [];
    foreach ($sort_options as $key => $option) {
      $options[$key] = $option['label'];
    }
    return $options;
  }

  /**
   * Returns the referenced entities for display.
   *
   * The method takes care of:
   * - checking entity access,
   * - placing the entities in the language expected for display.
   * It is thus strongly recommended that formatters use it in their
   * implementation of viewElements($items) rather than dealing with $items
   * directly.
   *
   * For each entity, the EntityReferenceItem by which the entity is referenced
   * is available in $entity->_referringItem. This is useful for field types
   * that store additional values next to the reference itself.
   *
   * @param \Drupal\Core\Field\EntityReferenceFieldItemListInterface $items
   *   The item list.
   * @param string $langcode
   *   The language code of the referenced entities to display.
   *
   * @return \Drupal\Core\Entity\EntityInterface[]
   *   The array of referenced entities to display, keyed by delta.
   *
   * @see ::prepareView()
   */
  public function getEntities(
    $items,
    $langcode
  ) {
    $entities = [];

    $excludes = [
      $this->getEntityFromRequest()->id(),
    ];
    foreach ($items as $delta => $item) {
      if ($item->value['mode'] === 'manual') {
        if ($item->value['uri'] !== '') {
          if ($this->isRoute($item->value['uri'])) {
            $params = $this->getRouteParameters($item->value['uri']);
            $excludes[] = $params[key($params)];
          }
        }
      }
    }

    foreach ($items as $item) {

      switch ($item->value['mode']) {

        case 'manual':

          if ($item->value['uri'] !== '') {
            if ($this->isRoute($item->value['uri'])) {
              $params = $this->getRouteParameters($item->value['uri']);
              $entity_type = key($params);
              $entity = \Drupal::service('entity_type.manager')
                ->getStorage($entity_type)
                ->load($params[$entity_type]);

              $entity = $this->getTranslatedEntity($entity, $langcode);

              if ($this->checkAccess($entity)->isAllowed() && $this->notItself($entity)) {
                $entities[] = $entity;
              }
            }
          }

          break;

        case 'automatic':

          $query = \Drupal::service('entity.query')
            ->get($item->value['type'])
            ->condition('type', $item->value['bundle'])
            ->range($item->value['offset'], $item->value['count']);

          foreach ($excludes as $exclude) {
            $query->condition('nid', $exclude, '!=');
          }

          $settings = $items->getItemDefinition()->getSettings();

          if ($settings['filter'] && $settings['filter']['taxonomy_term']) {
            foreach ($settings['filter']['taxonomy_term'] as $vocabulary => $type) {
              if ($type === 'select' && $item->value['filter'][$vocabulary] && $item->value['filter'][$vocabulary] !== '0') {
                $query->condition($vocabulary . '.target_id', $item->value['filter'][$vocabulary]);
              } else if ($type === 'autocomplete' && $item->value['filter'][$vocabulary]) {
                foreach ($item->value['filter'][$vocabulary] as $term) {
                  $query->condition($vocabulary . '.target_id', $term['target_id']);
                }
              }
            }
          }

          $sort = $this->getSort($item->value['sort']);
          $query->sort($sort['field'], $sort['direction']);

          $entity_ids = $query->execute();

          foreach (\Drupal::service('entity_type.manager')->getStorage($item->value['type'])->loadMultiple($entity_ids) as $entity) {

            $entity = $this->getTranslatedEntity($entity, $langcode);

            if ($this->checkAccess($entity)->isAllowed()) {
              $entities[] = $entity;
            }

          }

          break;

      }

//      $access = $this->checkAccess($entity);
//      // Add the access result's cacheability, ::view() needs it.
//      $item->_accessCacheability = CacheableMetadata::createFromObject($access);
//      if ($access->isAllowed()) {
//        // Add the referring item, in case the formatter needs it.
//        $entity->_referringItem = $items[$delta];
//        $entities[$delta] = $entity;
//      }

    }

    return $entities;
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityFromRequest() {
    // A potential entity will be found in the route parameters.
    if (($route = \Drupal::service('current_route_match')->getRouteObject()) && ($parameters = $route->getOption('parameters'))) {
      // Determine if the current route represents an entity.
      foreach ($parameters as $name => $options) {
        if (isset($options['type']) && preg_match('~^entity:~i', $options['type'])) {
          $entity = \Drupal::service('current_route_match')->getParameter($name);
          if ($entity instanceof ContentEntityInterface && $entity->hasLinkTemplate('canonical')) {
            return $entity;
          }
        }
      }
    }
  }

  public function getTranslatedEntity($entity, $langcode) {
    // Set the entity in the correct language for display.
    if ($entity instanceof TranslatableInterface) {
      $entity = \Drupal::entityManager()
        ->getTranslationFromContext($entity, $langcode);
    };

    return $entity;
  }

  /**
   * Checks access to the given entity.
   *
   * By default, entity 'view' access is checked. However, a subclass can choose
   * to exclude certain items from entity access checking by immediately
   * granting access.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to check.
   *
   * @return \Drupal\Core\Access\AccessResult
   *   A cacheable access result.
   */
  protected function checkAccess($entity) {
    return $entity->access('view', NULL, TRUE);
  }

  protected function notItself($entity) {
    $host = $this->getEntityFromRequest();
    return ($entity->id() !== $host->id() && $entity->getEntityTypeId() === $host->getEntityTypeId());
  }

  /**
   * @param $item
   *
   * @return array
   */
  protected function getRouteParameters($uri) {
    return Url::fromUri($uri)->getRouteParameters();
  }

  protected function isRoute($uri) {
    try {
      $params = Url::fromUri($uri)->getRouteParameters();
      return true;
    } catch (\Exception $e) {
      return false;
    }
  }

}
