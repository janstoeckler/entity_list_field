# Entity list field

Provides a new field type for listing/referencing entities. Lists of entities can be assembled manually using an entity reference widget as well as automatically by configuring some conditions for an [entity query](https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Entity%21entity.api.php/group/entity_api/).

## Dependencies

- [drupal/link](https://www.drupal.org/docs/8/core/modules/link/overview) (part of drupal/core)
- [drupal/jsonapi](https://www.drupal.org/project/jsonapi)